import React from 'react'
import { prefixLink } from 'gatsby-helpers'
import { Link } from 'react-router'

class techskills extends React.Component{

  render(){
    const feskills = ['UX & UI Design','Javascript', 'Angular', 'React', 'Vue', 'Sass/ Less/ PostCss/ Stylus', 'Handlebars', 'nunjucks', 'HTML5/ CSS3', 'Grunt/ Gulp', 'ejs', 'pug', 'liquid' ];
    const feskillsList = feskills.map(function(feskill){
                    return <li>{feskill}</li>;
                  });

    const beskills = ['Nodejs/ npm', 'Flux/ Redux', 'Python/ Django', 'PHP/ Laravel/ Symofony', 'Ruby/ Rails', 'MEAN', 'LAMP', 'AWS', 'Docker', 'Unit Testing'];
    const beskillsList = beskills.map(function(beskill){
                    return <li>{beskill}</li>;
                  });

    const oskills = ['Wordpress', 'Magento', 'Dugway', 'Spree/ Solidus/ Shopify', 'Illustrator', 'Photoshop', 'PRINCE2 Practitioner', 'Mobile', 'RWD'];
    const oskillsList = oskills.map(function(oskill){
                    return <li>{oskill}</li>;
                  });
    const PortWrapperTech = { height: '100%',
      width: '100%',
      textAlign: 'center',};
    const BoxWrapperTech = {
      minHeight: '200px',
      maxHeight: '400px',
      minWidth: '300px',
      maxWidth: '700px',
      background: '#f5f5f5',
      display: 'inline-block',
      margin: '0 auto',
      margin: '40px',};
    return(
      <div className="portfolio-wrapper portfolio-wrapper-skillz" style={PortWrapperTech}>
        <div className="portfolio-container" id="port-box-1" style={BoxWrapperTech}><h1>Front End</h1><a href="#openModal1">Learn More</a>

<div id="openModal1" className="modalDialog">
    <div>	<a href="#close" title="Close" class="close">X</a>

        	<h2>Front End Skills</h2>
          <ul>{feskillsList}</ul>
    </div>
</div></div>
        <div className="portfolio-container" id="port-box-2" style={BoxWrapperTech}><h1>Back End</h1><a href="#openModal2">Learn More</a>

<div id="openModal2" className="modalDialog">
    <div>	<a href="#close" title="Close" class="close">X</a>

        	<h2>Back End</h2>
          <ul>{beskillsList}</ul>
    </div>
</div></div>
        <div className="portfolio-container" id="port-box-3" style={BoxWrapperTech}><h1>Other</h1><a href="#openModal3">Learn More</a>

<div id="openModal3" className="modalDialog">
    <div>	<a href="#close" title="Close" class="close">X</a>

        	<h2>Other</h2>
          <ul>{oskillsList}</ul>
    </div>
</div></div>
      </div>
    )
  }
}

export default techskills;
