---
title: New Year New Site
date: "2017-02-03"
layout: post
path: "/blog/new-year/"
---

A new year a new website using [Gatsby](https://github.com/gatsbyjs/gatsby), which is built with react. After spending a while toying around with other static site generator's I wanted to use a react based SSG. After deliberating between Gatsby and Phenomic I thought I would try using Gatsby for now.

I'm also going to be taking the leap into (hopefully frequent) blogging. This will include rants and reflections, also maybe some pseduo tutorial/ cheat-sheet stuff. For my own reference (as well as anyone else). The general tone of these blogs will be noob centric for the time being, but we'll see how things progress over time.
