import React from 'react'
import { prefixLink } from 'gatsby-helpers'
import { Link } from 'react-router'
import Helmet from "react-helmet"

import '../css/about.scss'

class About extends React.Component {
  render () {
    const about = 'About Sean King';

    const titleBG = {
      // background: '#fff',
      // padding: '15px',
      // border: '5px solid #333',
    };

    const textBG = { background: '#fff',
      border: '5px solid darkviolet',};

    return (
      <div className="about">
        <Helmet
          title={about}
          meta={[
             {"name": "description", "content": "Sean King portfolio site"},
            {"name": "keywords", "content": "blog, development, engineer, web developer, react, gatsby, javascript"},
          ]}
        />
        <h4 style={titleBG}><a>who I am</a></h4>
        <div style={textBG}>
        <p>
          My name is Sean. I'm primarily a front-end engineer who has been morphing into full-stack development. More precisely I find myself venturing into full-stack javascript development due to the ubiquity of Node.js.
          <br/>

          <br/>
          I also like Skateboarding, Heavy Metal, Magic: the gathering and Coffee and I collect clicky pencils and guitars.
        </p>

        </div>

         <h4 style={titleBG}><a>what I do</a></h4>
         <div style={textBG}>
         <p>
          I build websites and apps. At the moment I like using Django and React, as well as Angular and Vue depending on what a project needs.
          I like working with both MEAN and LAMP stacks and deploy on github and AWS where possible.
          I'm comfortable with creating Wordpress sites and wrangling child themes.
          <br/><br/> I know how to waltz with PHP and tango with Ruby and wrestle with Python and I can create bespoke themes for Big Cartel, Shopify, Magento and Woocommerce solutions.
          <br/>
          <br/>I care deeply about responsive web design and employ a holistic and device agnostic approach to how I design and the technology I employ.
          <br/><br/>Ultimately, I like making things look good, while making sure users have an enjoyable and intuitive experience while using
          best and most appropriate technology for the project.
         </p>
         </div>
      </div>
    )
  }
}

export default About
