---
title: Emacs vs Vim
date: "2017-04-13"
layout: post
path: "/blog/saving/"
---

This old chestnut. I have spent the last couple of years toying with emacs. I keep on adding things I never need. I keep on tweaking and then it becomes an unwieldy beast. I now understand the joke where people say emacs is a great operating system that lacks a decent text editor. Emacs also takes ages to load up, more so each time I tweak my config, just a little, teeny, weeny bit more. Maybe I just don't know how to use it properly.

Then I'll get frustrated with the load time or something and then I normally delete my old dotfiles, get a copy of bbatsov's emacs starter kit from github and then start over again. And then I add just a few little extras like electric pair mode or rainbow mode. Each time I promise myself I'll keep it chill and simple. I swear that I'll keep it efficient and minimal. Bare necessities this time. Honestly. Then I delve into the interwebs to see where my emacs peeps are at and then I get excited by all the extra packages that people recommend "I totes need that to speed up productivity", I say to myself. And then the custom .el file grows and grows. It becomes unwieldy again, it becomes sentient. An autonomous entity that can code all by itself (seemingly)- no need for it's human counterpart (why else would the default key bindings feel so awkward?).

And then productivity slows down and so I jump onto atom or vscode to finish off a job, and then those gui editors feels all slow and clunky and a bit to GUI-ish and annoying. And then I get faint whisp of an idea "If all I really need right now is a light weight editor why don't I just get vim going? I can install everything easily through vundle, they key mapping always feels faster and more efficient... but... but I spent all that time learning to use emacs, learning to love emacs... well I'll just give vim a go for a week or so... and outside of work hours too...". And then everything changed. I tried out a few ready made vim configs on Github but then went au naturel and jut added my own plugins as I needed them. I find it easy not to go overboard with extras, and also it is so, so quick to load up. And much kinder on my fingers too. I first I felt ashamed. But then I felt relieved. And then I felt really happy. My producutivity went up and I started slowly lose my text editor/ IDE anxiety.

So that's it. That's how I left emacs for vim. for now...
