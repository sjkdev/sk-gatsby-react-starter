---
title: Github Hosting
date: "2017-04-17"
layout: post
path: "/blog/Github/"
---

Ahhh. Good old Github. I use GH-pages to deploy test sites and small projects all the time. It's a great tool for live testing and a/b testing for myself and for collaborating and giving clients a taste of what production ready solutions can look like. I usually keep reference notes/ cheat sheet stuff on my trello for how to set up a gh-pages project but for the sake of the blog I thought I would share the terminal commands I use here:


#### Initialise the repo
* git init

#### Add the files
* git add -a

#### Add the commit message
* git commit -m 'initial repo commit'

#### Add remote origin
* git remote add origin https://github.com/your-repo/your-project.git

#### Push the commit
* git push -u -f origin master

And there you go. If you run into any errors a quick search on google or stackoverflow will sort you out.
