import React from 'react'
import { prefixLink } from 'gatsby-helpers'
import { Link } from 'react-router'

import '../css/about.scss'

class List extends React.Component {

    render() {
        const names = ['Jake', 'Jon', 'Thruster'];
        const namesList = names.map(function(name){
                        return <li>{name}</li>;
                      });
        const namey ={ width: '80vw',
          height: '65vh',
          margin: '0 auto',
          color: '#f5f5f5',};
        const listeypad = { paddingTop: '80px', };

        return  (
          <div id="namelist" style={namey}>
            <ul style={listeypad}>{ namesList }</ul>
          </div>
        )
    }
}

export default List;
