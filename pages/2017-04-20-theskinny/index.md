---
title: The Skinny
date: "2017-04-20"
layout: post
path: "/blog/theskinny"
---

The skinny. The low down. Mindless ramblings. Quixotic quintessence. "reverse the polarity of the neutron flow". "Would you like a jelly baby?". Practising the art of conversation can sometimes feel like rolling sticky food around in your mouth, if you're not accustomed to talking to that many people on a daily basis. Such is the life of a freelance web dev.

Blogging still feels a little unnatural. I have a list of topics that I think would be apt and hopefully over time it will feel more natural to talk about the things that I experience as a developer. The main thing I feel uncomfortable with is that I don't get to talk shop a great deal. I do intend to go to more meetups to converse with other devs, but being a freelancer I work in isolation a lot. I get to talk about business with clients and potential clients but I don't often get to talk code with my peers.

Hopefully blogging will help me articulate the more technical side of what I do. I think it is important for me to learn how to explain to do the things I do. Sometimes thinking out loud can help with planning or decision making. To verbalise and also substantiate what's on my to-do list or project tracker. Also, I am a firm believer in that clarity of thought reflects clarity of mind.

Another blog entry completed. **Note to self: future blogs should be based on specific technologies and specific real world problems/ solutions to do with such technologies**.
