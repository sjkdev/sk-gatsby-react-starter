import React from 'react'
import { prefixLink } from 'gatsby-helpers'
import { Link } from 'react-router'

class Portfolio extends React.Component{

  render(){
    const PortWrapper = { height: '100%',
      width: '100%',
      textAlign: 'center',};
    const BoxWrapper = {
      minHeight: '100px',
      maxHeight: '200px',
      minWidth: '300px',
      maxWidth: '400px',
      background: '#fff',
      display: 'inline-block',
      margin: '0 auto',
      margin: '0 40px 60px 40px',
      border: '5px solid darkviolet',
      textTransform: 'lowercase',};
    return(
      <div className="portfolio-wrapper" style={PortWrapper}>
        <div className="portfolio-container" id="port-box-1" style={BoxWrapper}><h1>Digital Wonderland</h1><a href="#openModal1">Learn More</a>

<div id="openModal1" className="modalDialog">
    <div>	<a href="#close" title="Close" class="close">X</a>

        	<h2>Digital Wonderland</h2>

        <p>This is a sample modal box that shows content in box onne</p>
        <p>You could do a lot of things here like have a pop-up ad that shows when your website loads, or create a login/register form for users.</p>
    </div>
</div></div>
        <div className="portfolio-container" id="port-box-2" style={BoxWrapper}><h1>Optik Nerve</h1><a href="#openModal2">Learn More</a>

<div id="openModal2" className="modalDialog">
    <div>	<a href="#close" title="Close" class="close">X</a>

        	<h2>Optik Nerve</h2>

        <p>blah blah blah two two two</p>
    </div>
</div></div>
        <div className="portfolio-container" id="port-box-3" style={BoxWrapper}><h1>Wyrd Fun</h1><a href="#openModal3">Learn More</a>

<div id="openModal3" className="modalDialog">
    <div>	<a href="#close" title="Close" class="close">X</a>

        	<h2>Wyrd Fun</h2>

        <p> bv jokgfnhdjosæ#tsrjoyer
        .</p>
    </div>
</div></div>
        <div className="portfolio-container" id="port-box-4" style={BoxWrapper}><h1>Wizard Status</h1><a href="#openModal4">Learn More</a>

<div id="openModal4" className="modalDialog">
    <div>	<a href="#close" title="Close" class="close">X</a>

        	<h2>Wizard Status</h2>

        <p>This is a sample modal box that can be created using the powers of CSS3.</p>
        <p>You could do a lot of things here like have a pop-up ad that shows when your website loads, or create a login/register form for users.</p>
    </div>
</div></div>
        <div className="portfolio-container" id="port-box-5" style={BoxWrapper}><h1>Sea Pagan</h1><a href="#openModal5">Learn More</a>

<div id="openModal5" className="modalDialog">
    <div>	<a href="#close" title="Close" class="close">X</a>

        	<h2>Sea Pagan</h2>

        <p>This is a sample modal box that can be created using the powers of CSS3.</p>
        <p>You could do a lot of things here like have a pop-up ad that shows when your website loads, or create a login/register form for users.</p>
    </div>
</div></div>
        <div className="portfolio-container" id="port-box-6" style={BoxWrapper}><h1>Btown Threads</h1><a href="#openModal6">Learn More</a>

<div id="openModal6" className="modalDialog">
    <div>	<a href="#close" title="Close" class="close">X</a>

        	<h2>Btown Threads</h2>

        <p>This is a sample modal box that can be created using the powers of CSS3.</p>
        <p>You could do a lot of things here like have a pop-up ad that shows when your website loads, or create a login/register form for users.</p>
    </div>
</div></div>
        <div className="portfolio-container" id="port-box-7" style={BoxWrapper}><h1>Norffas</h1><a href="#openModal7">Learn More</a>

<div id="openModal7" className="modalDialog">
    <div>	<a href="#close" title="Close" class="close">X</a>

        	<h2>Norffas</h2>

        <p>This is a sample modal box that can be created using the powers of CSS3.</p>
        <p>You could do a lot of things here like have a pop-up ad that shows when your website loads, or create a login/register form for users.</p>
    </div>
</div></div>
        <div className="portfolio-container" id="port-box-8" style={BoxWrapper}><h1>Saffron Reichenbacker</h1><a href="#openModal8">Learn More</a>

<div id="openModal8" className="modalDialog">
    <div>	<a href="#close" title="Close" class="close">X</a>

        	<h2>Saffron Reichenbacker</h2>

        <p>This is a sample modal box that can be created using the powers of CSS3.</p>
        <p>You could do a lot of things here like have a pop-up ad that shows when your website loads, or create a login/register form for users.</p>
    </div>
</div></div>
        <div className="portfolio-container" id="port-box-9" style={BoxWrapper}><h1>Zoe Loveday Jewellery</h1><a href="#openModal9">Learn More</a>

<div id="openModal9" className="modalDialog">
    <div>	<a href="#close" title="Close" class="close">X</a>

        	<h2>Zoe Loveday Jewellery</h2>

        <p>This is a sample modal box that can be created using the powers of CSS3.</p>
        <p>You could do a lot of things here like have a pop-up ad that shows when your website loads, or create a login/register form for users.</p>
    </div>
</div></div>

      </div>
    )
  }
}

export default Portfolio;
