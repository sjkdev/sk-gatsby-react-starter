import React from 'react';
import { prefixLink } from 'gatsby-helpers';
import { Link } from 'react-router';



class techskills extends React.Component{

  render(){
    const feskills = ['Javascript', 'Angular/ React/ Vue', 'Sass/ Less/ PostCss/ Stylus', 'Handlebars, nunjucks, ejs, pug, liquid, erb', 'HTML5/ CSS3', 'Grunt/ Gulp/ Npm/ Webpack'];
    const feskillsList = feskills.map(function(feskill){
                    return <li>{feskill}</li>;
                  });

    const beskills = ['Nodejs', 'Flux/ Redux', 'Python/ Django', 'PHP/ Laravel/ Symofony', 'Ruby/ Rails', 'MEAN/ LAMP', 'AWS', 'mysql/ postgresql/ mongoDB/ Redis', 'Docker', 'Unit Testing', 'Linux'];
    const beskillsList = beskills.map(function(beskill){
                    return <li>{beskill}</li>;
                  });

    const oskills = ['Github', 'Wordpress', 'Magento', 'Big Cartel Themes/ Dugway', 'Spree/ Solidus/ Shopify', 'Mobile Apps/ react-native/ ionic', 'Java', 'C/ C++/ C#',];
    const oskillsList = oskills.map(function(oskill){
                    return <li>{oskill}</li>;
                  });

    const Designskills = ['Wireframing', 'UI Design', 'Illustrator', 'Photoshop', 'InDesign', 'Gimp', 'Inkscape', 'Vectr', 'Responsive Web Design', 'Logo Design', 'Art Direction'];
    const DesignskillsList = Designskills.map(function(Designskills){
                    return <li>{Designskills}</li>;
                  });

    const Businessskills = ['UX Design', 'Digital Business Strategy', 'SEO', 'PRINCE2 Practitioner/ Project Manager', 'Lean/ Agile Methodologies', 'Business workflow lifecycle'];
    const BusinessskillsList = Businessskills.map(function(Businessskills){
                    return <li>{Businessskills}</li>;
                  });

    const PortWrapperTech = {
      height: '100%',
      width: '100%',
      textAlign: 'center',
      overflow: 'scroll',};

    const BoxWrapperTech = {
      textAlign: 'center',
      minWidth: '300px',
      background: '#333',
      display: 'inline-block',
      color: '#f5f5f5',
      margin: '0 auto',
      textDecoration: 'none',
      listStyleType: 'none',
      border: '5px solid #ffff00',};

    const boxWrappers = {
      display: 'flex',
      margin: '40px',
      background: '#fff',
      border: '5px solid darkviolet',
      textAlign: 'center',
      marginBottom: '100px',
      float: 'left',};

    const primaryBox = { width: '300px',
      height: '200px',
      background: 'white!important',};

    const h1steez = { marginTop: '2rem',};

    const uberboxwrapper = { display: 'inline-block',};

    const modalscroll = { overflow: 'scroll',};

    const h1pad = { paddingTop: '-20px',};
    // const buttonsteez = { background: 'red',
    //   borderRadius: '20px',
    //   border: '5px solid #0000ff',};
    //
    //   function edit (){
    //       alert('Here is an alert');
    //   };

    return(
      <div className="portfolio-wrapper portfolio-wrapper-skillz" style={PortWrapperTech}>
      <div style={uberboxwrapper}>
      <div className="boxuno" style={boxWrappers}>
        <div className="portfolio-container-fe" id="port-box-1" style={BoxWrapperTech} style={primaryBox}><h1 style={h1pad}>Development</h1><a href="#openModal">Learn More</a>
          <div id="openModal" className="modalDialog">
            <div style={modalscroll}>	<a href="#close" title="Close" class="close">X</a>
        	   <h2>Development Skills</h2>
              <ul>{feskillsList}</ul>
              <ul>{beskillsList}</ul>
             <ul>{oskillsList}</ul>
            </div>
          </div>
        </div>
      </div>

      <div className="boxduo" style={boxWrappers}>
        <div className="portfolio-container-be" id="port-box-2" style={BoxWrapperTech} style={primaryBox}><h1 style={h1pad}>Design</h1><a href="#openModal1">Learn More</a>
          <div id="openModal1" className="modalDialog">
            <div style={modalscroll}>	<a href="#close" title="Close" class="close">X</a>
        	   <h2>Design Skills</h2>
              <ul>{DesignskillsList}</ul>
            </div>
          </div>
        </div>
      </div>
      <div className="boxtres" style={boxWrappers}>
        <div className="portfolio-container-ol" id="port-box-3" style={BoxWrapperTech} style={primaryBox}><h1 style={h1pad}>Strategy</h1><a href="#openModal2">Learn More</a>
          <div id="openModal2" className="modalDialog">
            <div style={modalscroll}>	<a href="#close" title="Close" class="close">X</a>
        	   <h2>Strategy and Planning</h2>
              <ul>{BusinessskillsList}</ul>
            </div>
          </div>
        </div>
      </div>
      </div>
    </div>
    )
  }
}

export default techskills;
