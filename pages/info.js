import React from 'react'
import { prefixLink } from 'gatsby-helpers'
import { Link } from 'react-router'
import Helmet from "react-helmet"

import '../css/about.scss'

class About extends React.Component {
  render () {
    const listcontainer = { width: '80vw',
      margin: '0 auto', };
    const fe = ['Jake', 'Jon', 'Thruster'];
    const feList = fe.map(function(fename){
                    return <li>{fename}</li>;
                  });
    const be = ['mean', 'lamp', 'docker'];
    const beList = be.map(function(bename){
                    return <li>{bename}</li>;
                  });
    const OL = ['trello', 'devops', 'CC'];
    const OList = OL.map(function(OLname){
                    return <li>{OLname}</li>;
                  });
    const ML = ['PRINCE2', 'SQL', 'Redis'];
    const MList = ML.map(function(MLname){
                    return <li>{MLname}</li>;
                  });
    return (
      <div className="info-wrapper"style={listcontainer}>
        <div className="info-title-pos">
          <h1>Skills</h1>
          <div>
            <h1>Front end</h1>
            <ul style={feList}></ul>
          </div>
          <div>
            <h1>Back end</h1>
            <ul style={beList}></ul>
          </div>
          <div>
            <h1>Other</h1>
            <ul style={OList}></ul>
          </div>
          <div>
            <h1>misc</h1>
            <ul style={MList}></ul>
          </div>
        </div>
      </div>

    )
  }
}

export default About
