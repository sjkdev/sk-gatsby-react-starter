import React from 'react';
import { Link } from 'react-router';
import { prefixLink } from 'gatsby-helpers';
import './nav.scss';
import klogo from './grey-logo-sm.png';

class Navigation extends React.Component {
  render() {
    const klogoposition = { position: 'absolute',
      left: '25px',
      paddingTop: '25px',};
    return (
      <div className="nav">
        <Link className='nav__link' to={prefixLink('/')} style={klogoposition}><img id="klogo-img" alt="img-logo" src={klogo} /></Link>
        <Link className='nav__link' to={prefixLink('/about/')}>about</Link>
        <Link className='nav__link' to={prefixLink('/blog/')}>blog</Link>
        <Link className='nav__link' to={prefixLink('/techlist-og/')}>tech</Link>
        <Link className='nav__link' to={prefixLink('/projects/')}>projects</Link>
      </div>
    )
  }
}

export default Navigation;
