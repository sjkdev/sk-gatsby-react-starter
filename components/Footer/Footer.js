import React from 'react';
import { prefixLink } from 'gatsby-helpers';
import Link from 'react-router';
import { FaTwitter, FaGithub, FaLinkedin } from 'react-icons/lib/fa';
import './footer.scss';

class Footer extends React.Component {
  render () {
    return (
      <div className='footer'>
        <a href="mailto:sean@digitalwonderlandagency.co.uk">sean@digitalwonderlandagency.co.uk</a>
        <div className="icons">
          <a href='https://twitter.com/digital_wonder' target='_blank'>
          <FaTwitter />
        </a>
	      <a href='https://github.com/themezzilla' target='_blank'>
          <FaGithub />
        </a>
        <a href='https://linkedin.com' target='_blank'>
          <FaLinkedin />
        </a>
        </div>
        <p>© 2017 Sean King</p>
      </div>
    )
  }
}

export default Footer;
