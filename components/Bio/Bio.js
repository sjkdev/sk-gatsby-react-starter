import React from 'react';
import { config } from 'config';
import { prefixLink } from 'gatsby-helpers';
import profilePic from './sean.png';
import { Link } from 'react-router';
import './bio.scss';
import * as FontAwesome from 'react-icons/lib/fa';
import { FaArrowCircleODown } from 'react-icons/lib/fa';

class Bio extends React.Component {
  render () {
    return (
      <div className='bio'>
        <div className="biodeets bio-h2">
        <div className="atf-section">
        <h2>Hi!</h2>
        <br/>
        <br/>
        <h2>My name is Sean</h2>

        <br/>
        <br/>
        <br/>
        <div className="icons">
        <h2>I am a fullstack developer <FaArrowCircleODown /></h2>
        </div>
        </div>
        <br/>
        <br/>
        <br/>
        <br/>
        <h2>I am also a UI/UX Designer </h2>
        <h3 className="tacenter">with a passion for designing beautiful and functional digital experiences</h3>
        <br/>
        <br/>
        <br/>
        <h2>I guess I would say I am a professional Nerd</h2>
        <br/>
        <br/>
        <br/>
        <h2>I design and develop cool stuff on the interwebs for <br/><Link to={'http://www.digitalwonderlandagency.co.uk'} target='_blank'>Digital Wonderland</Link></h2>
        <br/>
        <br/>
        <br/>
        <h6>Obligatory footnote to let you know that this site was built with react/ redux, webpack etc</h6>

          <p>
           Read <Link to='/about/'>about</Link> me, check out my <Link to='/blog/'>blog</Link> or download <a href="/resume.pdf">my resume</a>.
          </p>
        </div>
      </div>
    )
  }
}

export default Bio;
/*
  <img className='me' src={profilePic} alt={`author ${config.authorName}`} />
*/
