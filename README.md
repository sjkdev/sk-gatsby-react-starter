# portfolio-gatsby
Portfolio site complete with blog, built with ReactJS static generator gatsby.

### Installation
`$ yarn`

### Local dev server with livereload
`$ gatsby develop`