Here is some left overs which didn't seem appropriate on the _**about page**_ but seems good fodder for a blog entry.

For fun I like to play around with P5.js/ processing.js and tinkering with static site generators like Hugo/ Hexo/ Gatsby/ Phenomic etc.

*When it comes to my set up I'm a bit of an obsessive and I'm always tweaking my editor. I used to be an emacs sort of dude and then I switched over to vim and am currently geeking out with that. I use Arch Linux for my main set up and have tweaked my terminal to perfection with zsh. All of this, of course, gets revised and rejigged on a frequent basis and often on a whim - that's half the fun of it.*

*Even though I write code for a living it is still my main/ favourite hobby and have found myself becoming a default serial entrepreneur - I will often invent companies in order to create a new website or app for them (quite often just to use a particular language or framework or to learn a new technology).*
